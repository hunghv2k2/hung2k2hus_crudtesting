package service

import (
	"Gogood/model"
	"context"
)

type ReadRestaurantsRepo interface {
	Read(ctx context.Context, id int) (*model.Restaurants, error)
}

type readRestaurantsService struct {
	repo ReadRestaurantsRepo
}

func NewReadRestaurantsService(repo ReadRestaurantsRepo) *readRestaurantsService {
	return &readRestaurantsService{repo: repo}
}

func (src *readRestaurantsService) ReadRestaurants(
	context context.Context,
	id int,
) (*model.Restaurants, error) {
	data, err := src.repo.Read(context, id)

	if err != nil {
		return nil, err
	}

	return data, nil
}
