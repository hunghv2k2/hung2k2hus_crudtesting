package main

import (
	"Gogood/app/appctx"
	"Gogood/transport/CRUD_restaurants"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
)

type Restaurants struct {
	Id      int    `json:"id" gorm:"column:id;"`
	Name    string `json:"name" gorm:"column:name;"`
	Address string `json:"addr" gorm:"column:addr;"`
	//CreatedAt *time.Time `json:"created_at" gorm:"column:created_at;"`
	//UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at;"`
}

func (Restaurants) TableName() string { return "restaurants" }

func main() {
	dsn := "root:3108@tcp(127.0.0.1:3306)/food_delivery?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalln("Cannot connect to MySQL:", err)
	}

	log.Println("Connected to MySQL:", db)

	router := gin.Default()
	appctx := appctx.NewAppContext(db)

	v1 := router.Group("/v1")
	{
		v1.POST("/items", CRUD_restaurants.CreateRestaurant(appctx))
		v1.GET("/items/:id", CRUD_restaurants.ReadRestaurant(appctx))
		v1.PUT("/items/:id", CRUD_restaurants.UpdateRestaurant(appctx))
		v1.DELETE("/items/:id", CRUD_restaurants.DeleteRestaurant(appctx))
	}

	router.Run()
}
