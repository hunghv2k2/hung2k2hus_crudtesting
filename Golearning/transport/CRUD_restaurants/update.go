package CRUD_restaurants

import (
	"Gogood/app/appctx"
	"Gogood/model"
	"Gogood/repository"
	"Gogood/service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func UpdateRestaurant(appCtx appctx.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		db := appCtx.GetDBConnect()

		id, err := strconv.Atoi(c.Param("id"))

		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		var data model.RestaurantsUpdate

		if err := c.ShouldBind(&data); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		repo := repository.NewSqlRepo(db)
		src := service.NewUpdateRestaurantsService(repo)

		errs := src.UpdateRestaurants(c.Request.Context(), &data, id)

		if errs != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})

			return
		}
		c.JSON(http.StatusOK, gin.H{
			"data": true,
		})
	}
}
