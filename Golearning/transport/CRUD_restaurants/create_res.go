package CRUD_restaurants

import (
	"Gogood/app/appctx"
	"Gogood/model"
	"Gogood/repository"
	"Gogood/service"
	"github.com/gin-gonic/gin"
	"net/http"
)

func CreateRestaurant(appCtx appctx.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {
		db := appCtx.GetDBConnect()
		var data model.RestaurantsCreate

		if err := c.ShouldBind(&data); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})

			return
		}

		repo := repository.NewSqlRepo(db)
		src := service.NewCreateRestaurantsService(repo)

		if err := src.CreateRestaurants(c.Request.Context(), &data); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})

			return
		}

		c.JSON(http.StatusOK, gin.H{
			"data": data,
		})
	}
}
