package CRUD_restaurants

import (
	"Gogood/app/appctx"
	"Gogood/repository"
	"Gogood/service"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func ReadRestaurant(appCtx appctx.AppContext) gin.HandlerFunc {
	return func(c *gin.Context) {

		db := appCtx.GetDBConnect()

		id, err := strconv.Atoi(c.Param("id"))

		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		repo := repository.NewSqlRepo(db)
		src := service.NewReadRestaurantsService(repo)

		data, err := src.ReadRestaurants(c.Request.Context(), id)

		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err.Error(),
			})

			return
		}
		c.JSON(http.StatusOK, gin.H{
			"data": data,
		})
	}
}
