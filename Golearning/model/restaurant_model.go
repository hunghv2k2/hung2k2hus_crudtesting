package model

type Restaurants struct {
	Id      int    `json:"id" gorm:"column:id;"`
	Name    string `json:"name" gorm:"column:name;"`
	Address string `json:"addr" gorm:"column:addr;"`
	//CreatedAt *time.Time `json:"created_at" gorm:"column:created_at;"`
	//UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at;"`
}

func (Restaurants) TableName() string { return "restaurants" }

type RestaurantsCreate struct {
	Id      int    `json:"id" gorm:"column:id;"`
	Name    string `json:"name" gorm:"column:name;"`
	Address string `json:"addr" gorm:"column:addr;"`
}

func (RestaurantsCreate) TableName() string { return Restaurants{}.TableName() }

type RestaurantsUpdate struct {
	Name    string `json:"name" gorm:"column:name;"`
	Address string `json:"addr" gorm:"column:addr;"`
}

func (RestaurantsUpdate) TableName() string { return Restaurants{}.TableName() }
